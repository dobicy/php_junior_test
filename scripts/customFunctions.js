// Used to alert the response to the user
function getResponse(response) {
    if (response !== '') {
        alert(response);
    }
}

// Function to display all products as a Bootstrap cards
function displayProductList(product_attributes, products) {
    // product_attributes - products attribute values as an array of JS objects
    // products - information about the products as an array of JS objects

    if (product_attributes !== '' && products !== '') {
        product_attributes = JSON.parse(product_attributes);
        products = JSON.parse(products);
    } else return;

    let sku_attrvalue = {}; // Dictionary of SKU => AttributeValue for easy access to the values
    for (let row of product_attributes) {
        sku_attrvalue[row['SKU']] = row['AttributeValue'];
    }

    let main_div = $('#div_main'); // ID of Element, where cards will be created
    let i = 0; // Needed to keep track, when to create a new row for cards (4 cards per row)
    for (let product of products) {
        if (i === 0 || i % 4 === 0) {  // If there are no rows or a row is filled, create a new one
            main_div.append('<div class="row"></div>');
        }
        ++i;
        let row = main_div.children().last(); // Selects the last row
        row.append('<div class="col-sm-3 mb-3"></div>'); // Creates a new column, where a new card will be created
        let card = row.children().last(); // Selects last created column

        // HTML code to create a new Bootsrap card
        let card_html = "<div class='card text-center'>";
        card_html += "<div class='card-body'>";
        card_html += "<input class='checkbox' type='checkbox' name='checked_SKU[]' value=" + product['SKU'] + ">";
        card_html += "<p class='card-text'>" + product['SKU'] + "</p>";
        card_html += "<p class='card-text'>"+ product['Name'] +"</p>";
        card_html += "<p class='card-text'>"+ product['Price'] +" $</p>";
        card_html += "<p class='card-text\'>" +
            product['SpecialAttributeName']+": " + sku_attrvalue[product['SKU']] +
            ((product['UnitOfMeasurement'] === null) ? '' : ' ' + product['UnitOfMeasurement']) + "</p>";
        card_html += "</div>";
        card_html += "</div>";

        card.html(card_html); // Appending HTML code to the last created column
    }
}

// Function for dynamic type changing. Also restores the value of selected type after submission/page reload
function dynamicTypeChanger (post_select, select_error) {
    // post_select - The value of product type after submission
    // select_error - Status of product type. Is: '' - there is no error, '1' - there is error in selector value

    let select_id = $('#select_type'); // ID of the selector of product type

    // Restores the selected type after first form submission
    if (post_select !== '' && select_error === '') { // If product type was send and it has no errors
        select_id.val(post_select); // Restoring value for the selector
        let selected_div = "#div_type_" + post_select; // String corresponds to the div element of according type
        $(selected_div).show();

    } else {
        // Shows the selected type if page was reloaded without previous submission
        let selected_div = "#div_type_" + select_id.val();
        if ($(selected_div).length !== 0) $(selected_div).show(); // If selected div exists, show it
    }


    // Removes Bootstrap validation highlighting for the type fields that are not related to the selected type
    let non_related_inputs = $(".div_type input").not($("#div_type_" + select_id.val()).find('input'));
    non_related_inputs.removeClass('is-valid is-invalid');

    // Changing the visibility product types, when product type is switched
    select_id.on('change', function() {
        $(".div_type").hide();  // Hide all divs of product types
        $("#div_type_" + select_id.val()).show(); // Show the div of selected product type
    });
}