<?php

// Used to implement the logic of submission in 'product_add.php'
class FormSubmitter {
    private $conn_error;
    private $validator;
    private $product_db;

    public function connectToDB() {
        $this->product_db = new Database();
        $this->conn_error = $this->product_db->checkConnection();
    }

    public function goodDBConnection() {
        return !($this->conn_error);
    }

    public function validateFormFields() {
        $this->validator = new NewDataValidator();

        $this->validator->validateSKU($_POST['sku']);
        $this->validator->validateName($_POST['name']);
        $this->validator->validatePrice($_POST['price']);
        $this->validator->validateType($_POST['select_type'], $_POST);
    }

    public function validFormFields() {
        return (sizeof($this->validator->errors) === 0);
    }

    public function submitData() {
        $attribute_names = $this->validator->getAttributeNames(); // Get attribute names for the selected type

        $this->product_db->addNewProductRecord($_POST['sku'], $_POST['name'], $_POST['price'],
            $_POST['select_type']);
        foreach ($attribute_names as $attribute_name) {
            $this->product_db->addNewAttributeRecord($_POST['sku'], $attribute_name, $_POST[$attribute_name]);
        }
    }

    public function getValidationErrors() {
        return $this->validator->errors;
    }
}
