<?php

require_once 'classes/Database.php';
require_once 'abstract/Product.php';
require_once 'classes/product_types/DVD_Disc.php';
require_once 'classes/product_types/Furniture.php';
require_once 'classes/product_types/Book.php';

// Validates the data from page 'product_add.php'
class NewDataValidator {
    private static $product_types = array('Book', 'DVD-disc', 'Furniture');  // Names of accepted for validation types
    private $product; // Object for validation of selected product type values
    public $errors = array(); // Array 'form field name' => 'caught error'

    // Validation of type. If type is valid, continues with validation of the value of selected type
    public function validateType($type, $post_data) {
        if(empty($type)) {
            $this->addError('select_type', 'Type should be chosen.');
            return;
        }

        if(!in_array($type, self::$product_types)) {
            $this->addError('select_type', 'Selected type is not valid. Please choose the type from the
                provided ones.');
            return;
        } else {
            if ($type === 'Book') $this->product = new Book($post_data);
            if ($type === 'DVD-disc') $this->product = new DVD_Disc($post_data);
            if ($type === 'Furniture') $this->product = new Furniture($post_data);

            $this->product->validateTypeValues();

            $this->errors = array_merge($this->errors, $this->product->errors);
        }
    }

    public function validateSKU($sku) {
        $sku = trim($sku);

        if (empty($sku)) {
            $this->addError('sku', 'SKU cannot be left empty.');
            return;
        }

        if ($this->notInRange(strlen($sku), 1, 20)) {
            $this->addError('sku', 'SKU should be between 1 and 20 characters.');
            return;
        }

        if (!preg_match('/^[a-zA-Z0-9]*$/', $sku)) {
            $this->addError('sku', 'SKU should contain only alphanumeric characters.');
            return;
        }

        $product_db = new Database();
        if (!$product_db->skuIsUnique($sku)) {
            $this->addError('sku', 'Item with the same SKU already exists. Please, choose another SKU.');
            $product_db = null;
            return;
        }
        $product_db = null;
    }

    public function validateName($name) {
        $name = trim($name);

        if (empty($name)) {
            $this->addError('name', 'Name cannot be left empty.');
            return;
        }

        if ($this->notInRange(strlen($name), 1, 60)) {
            $this->addError('name', 'Name should be between 1 and 60 characters.');
            return;
        }
    }

    public function validatePrice($price) {
        $price = trim($price);

        if (empty($price) and !is_numeric($price)) {
            $this->addError('price', 'Price cannot be left empty.');
            return;
        }

        if (!preg_match('/^(0|[1-9]\d*).\d{2}$/', $price)) {
            $this->addError('price', 'Price should be in the format dollars.cents (ex: 3.00;0.99).');
            return;
        }

        if ($this->notInRange(floatval($price), 0, 999999.99)) {
            $this->addError('price', 'Price value should be between 0 and 999999.99.');
            return;
        }
    }

    // Method to add errors as 'field name' => 'error' pairs
    private function addError($key, $val) {
        $this->errors[$key] = $val;
    }

    private function notInRange($val, $min, $max) {
        return !($val >= $min && $val <= $max);
    }

    public function getAttributeNames() {
        return $this->product->attribute_names;
    }
}