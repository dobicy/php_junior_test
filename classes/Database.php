<?php
// Class to operate on a database 'shopDB'
class Database
{
    private $connection="";

    // Fill in the information about the server and user to able to connect to the database 'shopDB'
    private $servername = "";
    private $username = "";
    private $password = "";
    private $database= "shopDB";

    public function __construct() {
        $this->connection=new mysqli($this->servername, $this->username,$this->password,$this->database);
    }

    public function checkConnection() {
        $error = $this->connection -> connect_errno;
        return $error;
    }

    public function addNewProductRecord($sku, $name, $price, $type) {
        $sql = "INSERT INTO Products(SKU, Name, Price, TypeName) VALUES (?, ?, ?, ?)";
        $stmt = $this->connection->prepare($sql);
        $stmt->bind_param('ssds', $sku, $name, $price, $type);
        $stmt->execute();
    }

    public function addNewAttributeRecord($sku, $attribute_name, $attribute_value) {
        $sql = "INSERT INTO ProductAttributes(SKU, AttributeName, AttributeValue) VALUES (?, ?, ?)";
        $stmt = $this->connection->prepare($sql);
        $stmt->bind_param('ssd', $sku, $attribute_name, $attribute_value);
        $stmt->execute();
    }

    public function getListOfProducts() {
        $sql = "SELECT JSON_OBJECT(
            'SKU', P.SKU,
            'Name', P.Name,
            'Price', P.Price, 
            'SpecialAttributeName', T.SpecialAttributeName,
            'UnitOfMeasurement', T.UnitOfMeasurement) AS 'Result'
                FROM Products P, ProductTypes T WHERE P.TypeName = T.TypeName";
        $list = $this->connection->query($sql);
        return $list;
    }

    public function getListOfProductsAttributes() {
        // The special attribute "Dimensions" consists of 3 parts, so it was necessary to create separate SELECT
        // And combine with the regular one by using UNION
        $sql = "SELECT JSON_OBJECT(
               'SKU', A.SKU,
               'AttributeValue', A.AttributeValue) AS 'Result'
                FROM ProductAttributes A, Products P, ProductTypes T WHERE A.SKU = P.SKU AND P.TypeName = T.TypeName
                    AND T.SpecialAttributeName != 'Dimensions'
                UNION
                SELECT JSON_OBJECT(
                    'SKU', P.SKU,
                    'AttributeValue', CONCAT_WS('x', A1.AttributeValue, A2.AttributeValue, A3.AttributeValue))
                    AS 'Result'
                FROM ProductAttributes A1, ProductAttributes A2, ProductAttributes A3, Products P, ProductTypes T
                WHERE A1.SKU = A2.SKU AND A2.SKU = A3.SKU AND A3.SKU = P.SKU AND P.TypeName = T.TypeName
                    AND T.SpecialAttributeName = 'Dimensions'
                    AND A1.AttributeName = 'Height' AND A2.AttributeName = 'Width' AND A3.AttributeName = 'Length';";
        $list = $this->connection->query($sql);
        return $list;
    }

    public function massDeleteAction($sku_list) {
        $questionmarks = str_repeat("?, ", count($sku_list)-1) . "?"; // Placeholders for SKUs
        $types = str_repeat('s', count($sku_list));

        $stmt = $this->connection->prepare("DELETE FROM ProductAttributes WHERE SKU IN ($questionmarks)");
        $stmt->bind_param($types, ...$sku_list);
        $stmt->execute();

        $stmt = $this->connection->prepare("DELETE FROM Products WHERE SKU IN ($questionmarks)");
        $stmt->bind_param($types, ...$sku_list);
        $stmt->execute();
    }

    public function skuIsUnique($sku) {
        $sql = "SELECT SKU FROM Products WHERE SKU = '$sku'";
        $this->connection->query($sql);
        return ($this->connection->affected_rows === 0);
    }
}
