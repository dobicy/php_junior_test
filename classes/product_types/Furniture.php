<?php

class Furniture extends Product
{
    function __construct($post_data = array()) {
        $this->attribute_names = array('Height', 'Width', 'Length');

        $this->setProductTypeValues($post_data);
    }

    public function validateTypeValues() {
        $this->similarTypeValidation();
    }
}