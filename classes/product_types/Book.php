<?php

class Book extends Product
{
    function __construct($post_data = array()) {
        $this->attribute_names = array('Weight');

        $this->setProductTypeValues($post_data);
    }

    public function validateTypeValues() {
        $this->similarTypeValidation();
    }
}