<?php

class DVD_Disc extends Product
{
    function __construct($post_data = array()) {
        $this->attribute_names = array('Size');

        $this->setProductTypeValues($post_data);
    }

    public function validateTypeValues() {
        $this->similarTypeValidation();
    }
}