# Script to create the database 'shopDB'
CREATE DATABASE shopDB;
USE shopDB;

CREATE TABLE ProductTypes (
      TypeName nvarchar (60) NOT NULL PRIMARY KEY,
      SpecialAttributeName nvarchar (60) NOT NULL,
      UnitOfMeasurement nvarchar (30) NULL
);

# Filling the information of product types and their attributes
INSERT INTO ProductTypes VALUES ('Book', 'Weight', 'KG');
INSERT INTO ProductTypes VALUES ('Furniture', 'Dimensions', NULL);
INSERT INTO ProductTypes VALUES ('DVD-disc', 'Size', 'MB');

CREATE TABLE Products (
      SKU nvarchar (20) NOT NULL PRIMARY KEY,
      Name nvarchar (60) NOT NULL,
      Price decimal (8, 2) NOT NULL,
      TypeName nvarchar (60) NOT NULL,
      FOREIGN KEY (TypeName) REFERENCES ProductTypes(TypeName)
);

CREATE TABLE ProductAttributes (
       SKU nvarchar (20) NOT NULL,
       AttributeName nvarchar (60) NOT NULL,
       AttributeValue nvarchar (60) NOT NULL,
       FOREIGN KEY (SKU) REFERENCES Products(SKU),
       PRIMARY KEY (SKU, AttributeName)
);