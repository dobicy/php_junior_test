Before working/testing the pages, please, complete the following steps:

    Open MySQL shell and:
    1. Run script 'create_db.sql' to create the database 'shopDB'.
    2. Create a user and give him full access to the database 'shopDB'.
 
    Then open file 'Database.php' and fill in the information about 
    the server and the user you have created.

Also, the pages to test have the corresponding filenames:

    'product_add.php' - page to add a product
    'product_list.php' - page to view all existing products