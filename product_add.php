<?php
    // Display all errors
//    ini_set('display_errors', 1);
//    ini_set('display_startup_errors', 1);
//    error_reporting(E_ALL);

    require_once 'classes/NewDataValidator.php'; // For data validation
    require_once 'classes/Database.php'; // For DB connection
    require_once 'classes/FormSubmitter.php'; // For the form submission

    $title = "Product Add"; // Title of the page

    // Validation of submitted data
    if(isset($_POST['submit'])){
        $submitter = new FormSubmitter();

        $submitter->connectToDB();
        if ($submitter->goodDBConnection()) {
            $submitter->validateFormFields();

            if ($submitter->validFormFields()) {
                $submitter->submitData();

                $response[] = 'Submission was successful!';
                $_POST = [];
            } else {
                $errors = $submitter->getValidationErrors(); // Array 'form field name' => 'caught error'
                $response[] = 'Some fields are invalid. Submission has not happened.';
            }
        } else {
            $response[] = "Connection problems are present. Please, check/configure file 'Database.php'.";
        }

        $submitter = null;
    }

    // Function is used to restore values of form fields after invalid submission
    function returnPostData ($name) {
        if (isset($_POST['submit']))
            if (array_key_exists($name, $_POST)) echo htmlspecialchars($_POST[$name]);
    }

    // Function is used to set according styling (valid/invalid) for form fields
    function showInputStatus ($name) {
        global $errors;
        if (isset($errors) and isset($_POST['submit'])) {
            if (array_key_exists($name, $errors)) echo 'is-invalid';
            else echo 'is-valid';
        }
    }
?>

<?php require('page_parts/header.php'); ?>

<div id="main" class="container mt-4">
<!--    The header-->
    <div id="header" class="container row justify-content-between">
        <h1 id="h_header" class="col-sm-auto">Product Add</h1>

<!--        Submission button-->
        <button id="save_btn" form="add_form" value="submit" name="submit" type="submit"
                class="btn btn-primary col-sm-auto align-self-center">Save</button>
    </div>

    <hr class="mt-2 mb-4">

<!--    The form-->
    <form id="add_form" method="POST" action="">
<!--        SKU field-->
        <div class="form-group row">
            <label for="sku" class="col-sm-1 col-form-label text-center">SKU</label>
            <div class="col-sm-3">
                <input type="text" class="form-control <?php showInputStatus('sku'); ?>"
                       id="sku" name="sku" value="<?php returnPostData('sku') ?>">
            </div>

<!--            SKU error-->
            <div class="error col-sm-auto align-self-center">
                <?php echo $errors['sku'] ?? '' ?>
            </div>
        </div>

<!--        Name field-->
        <div class="form-group row">
            <label for="name" class="col-sm-1 col-form-label text-center">Name</label>
            <div class="col-sm-3">
                <input type="text" class="form-control <?php showInputStatus('name'); ?>"
                       id="name" name="name" value="<?php returnPostData('name') ?>">
            </div>

<!--            Name error-->
            <div class="error col-sm-auto align-self-center">
                <?php echo $errors['name'] ?? '' ?>
            </div>
        </div>

<!--        Price field-->
        <div class="form-group row">
            <label for="price" class="col-sm-1 col-form-label text-center">Price</label>
            <div class="col-sm-3">
                <input type="text" class="form-control <?php showInputStatus('price'); ?>"
                       id="price" name="price" value="<?php returnPostData('price'); ?>">
            </div>

<!--            Price error-->
            <div class="error col-sm-auto align-self-center">
                <?php echo $errors['price'] ?? '' ?>
            </div>
        </div>

<!--        Type selector-->
        <div class="form-group row mt-5">
            <label for="select_type" class="col-sm-auto col-form-label text-center">Type Switcher</label>
            <div class="col-sm-auto">
                <select id="select_type" name="select_type"
                        class="form-control <?php showInputStatus('select_type'); ?>">
                    <option selected value="">Type Switcher</option>
                    <option value="DVD-disc">DVD-disc</option>
                    <option value="Book">Book</option>
                    <option value="Furniture">Furniture</option>
                </select>
            </div>

<!--            Type error-->
            <div class="error col-sm-auto align-self-center">
                <?php echo $errors['select_type'] ?? '' ?>
            </div>
        </div>

<!--        Type: DVD-->
        <div id="div_type_DVD-disc" class="container mt-5 div_type">
            <div class="form-group row">
                <label for="Size" class="col-sm-1 col-form-label text-center">Size</label>
                <div class="col-sm-3">
<!--                    DVD size field-->
                    <input type="text" class="form-control <?php showInputStatus('Size'); ?>"
                           id="Size" name="Size" value="<?php returnPostData('Size') ?>">
                </div>

<!--                DVD size error-->
                <div class="error col-sm-auto align-self-center">
                    <?php echo $errors['Size'] ?? '' ?>
                </div>
            </div>

<!--            DVD type description-->
            <div class="description container mt-1">
                Please provide size in MB.
            </div>
        </div>

<!--        Type: Book-->
        <div id="div_type_Book" class="container mt-5 div_type">
            <div class="form-group row">
                <label for="Weight" class="col-sm-1 col-form-label text-center">Weight</label>
                <div class="col-sm-3">
<!--                    Book weight field-->
                    <input type="text" class="form-control <?php showInputStatus('Weight'); ?>"
                           id="Weight" name="Weight" value="<?php returnPostData('Weight') ?>">
                </div>

<!--                Book weight error-->
                <div class="error col-sm-auto align-self-center">
                    <?php echo $errors['Weight'] ?? '' ?>
                </div>
            </div>

<!--            Book type description-->
            <div class="description container mt-1">
                Please provide weight in Kg.
            </div>
        </div>

<!--        Type: Furniture-->
        <div id="div_type_Furniture" class="container mt-5 div_type">
            <div class="form-group row">
                <label for="Height" class="col-sm-1 col-form-label text-center">Height</label>
                <div class="col-sm-3">
<!--                    Furniture height field-->
                    <input type="text" class="form-control <?php showInputStatus('Height'); ?>"
                           id="Height" name="Height" value="<?php returnPostData('Height'); ?>">
                </div>

<!--                    Furniture height error-->
                <div class="error col-sm-auto align-self-center">
                    <?php echo $errors['Height'] ?? '' ?>
                </div>
            </div>

            <div class="form-group row">
                <label for="Width" class="col-sm-1 col-form-label text-center">Width</label>
                <div class="col-sm-3">
<!--                    Furniture width field-->
                    <input type="text" class="form-control <?php showInputStatus('Width'); ?>"
                           id="Width" name="Width" value="<?php returnPostData('Width') ?>">
                </div>

<!--                    Furniture width error-->
                <div class="error col-sm-auto align-self-center">
                    <?php echo $errors['Width'] ?? '' ?>
                </div>
            </div>

            <div class="form-group row">
                <label for="Length" class="col-sm-1 col-form-label text-center">Length</label>
                <div class="col-sm-3">
<!--                    Furniture length field-->
                    <input type="text" class="form-control <?php showInputStatus('Length'); ?>"
                           id="Length" name="Length" value="<?php returnPostData('Length') ?>">
                </div>

<!--                    Furniture length error-->
                <div class="error col-sm-auto align-self-center">
                    <?php echo $errors['Length'] ?? '' ?>
                </div>
            </div>

<!--            Furniture type description-->
            <div class="description container mt-1">
                Please provide dimensions in HxWxL format.
            </div>
        </div>

    </form>

</div>

<script>
    // Provides dynamic change of the type and restores the selected type value after invalid form submission
    dynamicTypeChanger(
        "<?php returnPostData('select_type'); ?>",
        "<?php if (isset($_POST['submit']) && isset($errors))
            if (array_key_exists('select_type', $errors))
                echo true;?>"
    );
</script>

<?php require('page_parts/footer.php'); ?>