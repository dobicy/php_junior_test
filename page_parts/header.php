<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $title; ?></title>
    <!--    Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="Daniels Buls">

    <!--    jQuery-->
    <script src="scripts/jquery_3.4.1.js"></script>
    <!--    Bootstrap-->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!--    Custom CSS-->
    <link rel="stylesheet" type="text/css"
          href="styles/style_<?php echo basename($_SERVER['SCRIPT_NAME'], '.php');?>.css">
    <!--    Custom JS-->
    <script src="scripts/customFunctions.js"></script>
</head>
<body>
