<?php
    // Display all errors
//     ini_set('display_errors', 1);
//     ini_set('display_startup_errors', 1);
//     error_reporting(E_ALL);

    require_once 'classes/Database.php'; // To get and process data from DB

    $title = "Product List"; // Title of the page

    $product_db = new Database(); // Creating a connection
    $conn_error = $product_db->checkConnection();

    // If mass_delete action is chosen, deletes selected products from DB
    if(isset($_POST['submit'])) {
        if (!$conn_error) {
            if(isset($_POST['select_action'])){
                $action = $_POST['select_action'];
                if ($action === "mass_delete" and isset($_POST['checked_SKU'])) {
                    $checked_SKU = $_POST['checked_SKU'];
                    $product_db->massDeleteAction($checked_SKU);
                }
            }

            $_POST = [];
        }
    }

    // Converts MySQL response to the text that is an array of JSON objects in JS syntax
    function mysqlToJson($mysql_response) {
        $rows = array();
        while($row = $mysql_response->fetch_assoc()) {
            $rows[] = $row['Result'];
        }
        $string_rows = implode(', ', $rows);

        echo '['.$string_rows.']';
    }

    // Gets needed info to display list of products
    if (!$conn_error) {
        $list_of_products = $product_db->getListOfProducts();  // Gets products info (except attribute values)
        $list_of_product_attributes =  $product_db->getListOfProductsAttributes(); // Gets products attribute values
    } else {
        $response[] = "Functionality of this webpage is unavailable. Please, check/configure file 'Database.php'.";
    }

    $product_db = null; // Closing the connection
?>

<?php require('page_parts/header.php'); ?>

<div class="container mt-4">
    <form id="main_form" method="POST" action="">
<!--        Header -->
        <div id="header" class="container row justify-content-between">
            <h1 class="col-sm-auto">Product List</h1>

<!--            Action selection and submission. -->
            <div id="action" class="form-inline col-sm-auto">
                <label for="select_action" class="sr-only">Action</label>
                <select id="select_action" class="form-control" name="select_action">
                    <option selected value="mass_delete">Mass Delete Action</option>
                </select>
                <button id="submit_btn" name="submit" value="submit" type="submit"
                        form="main_form" class="btn btn-primary">Apply</button>
            </div>
        </div>

        <hr class="mt-1 mb-4">

        <div id="div_main">
            <!--        Here are generated cards of products. -->
        </div>
    </form>
</div>

<script>
    // Display all products as a Bootstrap cards
    displayProductList(
        '<?php if(!$conn_error) mysqlToJson($list_of_product_attributes);?>',
        '<?php if(!$conn_error) mysqlToJson($list_of_products);?>'
    );
</script>

<?php include('page_parts/footer.php'); ?>