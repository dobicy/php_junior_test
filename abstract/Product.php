<?php

abstract class Product
{
    private $data = array(); // Storing all the data about the products
    public $errors = array();

    public function __get($name) {
        if(array_key_exists($name, $this->data)){
            return $this->data[$name];
        }

        $trace = debug_backtrace();
        trigger_error(
            'Undefined property via __get(): ' . $name .
            ' in ' . $trace[0]['file'] .
            ' on line ' . $trace[0]['line'],
            E_USER_NOTICE);
        return null;
    }

    public function __set($name, $value) {
        $this->data[$name] = $value;
    }

    protected function addError($key, $val) {
        $this->errors[$key] = $val;
    }

    protected function notInRange($val, $min, $max) {
        return !($val >= $min && $val <= $max);
    }

    protected function setProductTypeValues ($post_data = array()) {
        foreach($this->attribute_names as $attribute_name) {
            $this->{$attribute_name} = trim($post_data[$attribute_name]);
        }
    }

    // Method to validate such types as DVD-disc, Book or Furniture
    protected function similarTypeValidation () {
        foreach ($this->attribute_names as $attribute_name) {
            if(array_key_exists($attribute_name, $this->data)) {
                $name = $attribute_name;
                $value = $this->data[$name];

                if(empty($value) || !is_numeric($value)) {
                    $this->addError($name, $name.' cannot be left empty and is numeric.');
                    continue;
                }

                if (!preg_match('/^(0|[1-9]\d*)(.\d{1,3})?$/', $value)) {
                    $this->addError($name, $name.' value should be in format n{.xxx},
                    where n-number, x-digit and {.xxx} is optional part.');
                    continue;
                }

                if ($this->notInRange(floatval($value), 0, 999999.99)) {
                    $this->addError($name, $name.' value should be between 0 and 999999.99.');
                    continue;
                }
            }
        }
    }

    abstract public function validateTypeValues();
    abstract public function __construct($post_data = array());
}